//
//  SUEDeck.m
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import "SUEDeck.h"

@implementation SUEDeck

-(instancetype)init{
    self = [super init];

    if(self){
        _ordemSuits = [[NSArray alloc] initWithObjects:@"D", @"H", @"S", @"C", nil];
        _ordemValores = [[NSArray alloc] initWithObjects: @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K", nil]; //A=0 2=1 3=2...
    }
    
    return self;
}

-(void)setupCards
{
    int checker;
    _deck = [[NSMutableArray alloc] init];
    NSMutableDictionary* aux = [[NSMutableDictionary alloc]init];
    NSString* nome = @"";
    NSString* key = @"";
    
    for(int j = 0; j < 13;j++){
        nome = [self.ordemValores objectAtIndex:j];
        for(int k = 0; k < 4;k++){
            if(j!=9)
                nome = [nome substringToIndex:1];
            else
                nome = [nome substringToIndex:2];
            nome = [nome stringByAppendingString:[self.ordemSuits objectAtIndex:k]];
            SUECarta* cartaAuxiliar = [[SUECarta alloc]init];
            cartaAuxiliar.nome = nome;
            do{
                checker = arc4random();
                key = [NSString stringWithFormat:@"%d",checker];
            }while([aux objectForKey:key]!=nil);
            [aux setObject:cartaAuxiliar forKey:key];
        }
    }
    for(int i = 0;i < 52;i++){
        [self.deck addObject:[aux objectForKey:[aux.allKeys objectAtIndex:i]]];
    }
}

@end
