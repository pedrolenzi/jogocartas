//
//  SUECarta.m
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import "SUECarta.h"

@implementation SUECarta

-(NSString *)description{
    switch ([_nome characterAtIndex:0]){
        case 'A':_descricao =  @"Escolha alguem para tomar um shot.";
            break;
        case '2':_descricao =  @"Escolha duas pessoas para beber. Tambem pode-se escolher uma pessoa para beber duas vezes.";
            break;
        case '3':_descricao =  @"Apelido. Escolha um apelido para algum jogador, quem chama-lo por outra coisa bebe.\n(Duração: até o final do jogo)";
            break;
        case '4':_descricao =  @"Quando esse jogador cruzar os braços, todos devem cruzar tambem. O ultimo a fazer bebe.\n(Pode cruzar quando quiser)\n(Apenas um uso)";
            break;
        case '5':_descricao =  @"????";
            break;
        case '6':_descricao =  @"????";
            break;
        case '7':_descricao =  @"Cada jogador soma um em sua vez, sempre que o numero da soma seja um multiplo de 7, ou tenha 7 em algum digito deve-se falar 'PI' ou invez do numero. Se errar, bebe.\n(Ex. 6,PI,8...)";
            break;
        case '8':_descricao =  @"Crie uma regra. Toda vez que uma pessoa infringir, ela bebe.\n(Ex. Beber somente com a mão esquerda)\n(Duração: uma rodada)";
            break;
        case '9':_descricao =  @"Palavra proibida. Escolha uma palavra que toda a vez que alguem falar, quem falou bebe.\n(Duração: uma rodada)";
            break;
        case '1':_descricao =  @"'STOP': Escolha uma categoria e uma letra, o proximo jogador deve falar algo dessa categoria que comece com essa letra e assim sucessivamente até alguem repetir algum objeto ou não saber. (Ex. Carro com 'B')";
            break;
        case 'J':_descricao =  @"Homens bebem.";
            break;
        case 'Q':_descricao =  @"Mulheres bebem.";
            break;
        case 'K':_descricao =  @"Todos bebem.";
            break;
        
        default:return nil;
    }
    return _descricao;
}

@end
