//
//  main.m
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SUEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SUEAppDelegate class]));
    }
}
