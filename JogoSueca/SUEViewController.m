//
//  SUEViewController.m
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import "SUEViewController.h"

@interface SUEViewController ()
@property CGPoint initLocation;
@property UILabel *labelAnterior;
@property UILabel *labelProximo;
@property CGPoint curLocation;
@end

@implementation SUEViewController

static CGRect tamanhoCarta;
static CGPoint posicaoCentro;
static CGPoint posicaoFora;
static CGPoint posicaoForaEsquerda;
bool toqueValido;

- (void)viewDidLoad
{
    _animacaoFim = NO;
    posicaoForaEsquerda = CGPointMake(-400, 160);
    _deckPrefix = @"Deck Playing/"; //prefixo para os baralhos
    if([_deckPrefix isEqualToString:@"Deck Normal/"]){
        tamanhoCarta = CGRectMake(0, 0, 130, 202);
        posicaoCentro = CGPointMake(160, 160);
        posicaoFora = CGPointMake(400, 160);
    }
    else if([_deckPrefix isEqualToString:@"Deck Playing/"]){
        tamanhoCarta = CGRectMake(0, 0, 135, 202);
        posicaoCentro = CGPointMake(158, 160);
        posicaoFora = CGPointMake(404, 160);
    }

    
    [super viewDidLoad];
    _card = -1;
    _jogadas = 0;
    _deckOfCards = [[SUEDeck alloc]init];
    [_deckOfCards setupCards];
    UIImageView* backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poker.jpg"]];
    [self.view addSubview:backgroundImage];
    
    
    UIButton* startButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    startButton.frame = CGRectMake(0, 0, 100, 80);
    [startButton setTitle:@"Iniciar" forState:UIControlStateNormal];
    startButton.layer.cornerRadius = 21;
    startButton.layer.borderWidth = 1;
    startButton.center = CGPointMake(160, 180);
    startButton.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    startButton.titleLabel.font = [UIFont systemFontOfSize:25];
    [startButton addTarget:self action:@selector(startButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startButton];
    
    _imagemCartaCima = [[UIImageView alloc] initWithFrame:tamanhoCarta];
    _imagemCartaBaixo = [[UIImageView alloc] initWithFrame:tamanhoCarta];
    _labelJogadas = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    _labelDescricao = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 200)];
    
    _imagemCartaCima.center = posicaoFora;

    _imagemCartaBaixo.center = posicaoCentro;
    
    _labelJogadas.center = CGPointMake(160, 40);
    _labelJogadas.layer.borderWidth = 0.75;
    _labelJogadas.layer.cornerRadius = 10;
    _labelJogadas.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    _labelJogadas.text = [NSString stringWithFormat:@"%d/%d",_card+1,_jogadas+1];
    _labelJogadas.textAlignment = NSTextAlignmentCenter;
//    _labelJogadas.textColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    _labelDescricao.center = CGPointMake(160, 370);
    _labelDescricao.textAlignment = NSTextAlignmentCenter;
    _labelDescricao.text = @"Descrição das cartas ira aparecer aqui!";
    _labelDescricao.lineBreakMode = NSLineBreakByWordWrapping;
    _labelDescricao.numberOfLines = 7;
    _labelDescricao.layer.borderWidth = 0.75;
    _labelDescricao.font = [UIFont systemFontOfSize:21];
    _labelDescricao.layer.cornerRadius = 30;
    _labelDescricao.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
    
    _cartaCima = [[UIView alloc] initWithFrame:tamanhoCarta];
    _cartaBaixo = [[UIView alloc] initWithFrame:tamanhoCarta];
    
    _cartaCima.center = posicaoFora;
    _cartaBaixo.center = posicaoCentro;
    UIPanGestureRecognizer *panGesture =
    [[UIPanGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handlePanGesture:)];
    panGesture.delegate = self;
    _imagemCartaCima.userInteractionEnabled = YES;
    [_imagemCartaCima addGestureRecognizer:panGesture];
    
    [_cartaCima addSubview:_imagemCartaCima];
    [_cartaCima addSubview:_labelJogadas];
    
    [_cartaBaixo addSubview:_imagemCartaBaixo];
    [_cartaBaixo addSubview:_labelJogadas];
   [self.view addSubview:_labelDescricao]; //-----------------------------
    
    _labelAnterior = [[UILabel alloc] initWithFrame:CGRectMake(-25, 25, 150, 30)];
    _labelAnterior.text = @"Anterior";
    _labelAnterior.alpha = 0.f;
    _labelProximo = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 150, 30)];
    _labelProximo.text = @"Proxima";
    _labelProximo.alpha = 0.f;
    _labelAnterior.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:31];
    _labelProximo.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:31];
    _labelAnterior.textColor = [UIColor greenColor];
    _labelProximo.textColor = [UIColor greenColor];
    [_imagemCartaCima addSubview:_labelProximo];
    [_imagemCartaCima addSubview:_labelAnterior];
}


#pragma reconhecimento de gesture
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}


-(void)handlePanGesture:(UITapGestureRecognizer*)recognizer{
    if(recognizer.state == UIGestureRecognizerStateBegan){
        //UITouch* touch = [[touches allObjects]objectAtIndex:0];
        _initLocation = [recognizer locationOfTouch:0 inView:self.view]; // posicao onde o touch comecou
        if(_card > 0){
            _imagemCartaBaixo.image = [UIImage imageNamed:[_deckPrefix stringByAppendingString:[[_deckOfCards.deck objectAtIndex:_card-1] nome]]];
            _imagemCartaBaixo.hidden = NO;
        }
        else _imagemCartaBaixo.hidden = YES;
        if(_initLocation.x >= 160-(140/2)&&_initLocation.x <= 160+(140/2)&&_initLocation.y >= 160-(212/2)&&_initLocation.y <= 160+(212/2)){
            toqueValido = YES;
        }
        else toqueValido = NO;
        
    }
    if(recognizer.state == UIGestureRecognizerStateChanged){
        //UITouch* touch = [[touches allObjects]objectAtIndex:0];
        _curLocation = [recognizer locationOfTouch:0 inView:self.view];
        CGFloat difX = _curLocation.x - _initLocation.x;
        CGFloat difY = _curLocation.y - _initLocation.y;
        if(toqueValido){
            
            _imagemCartaCima.center = CGPointMake(_curLocation.x, _imagemCartaCima.center.y);
            _imagemCartaCima.transform = CGAffineTransformMakeRotation(0.003*difX);
            if (difX>50) _labelAnterior.alpha = (difX-50)/75;
            else         _labelAnterior.alpha = 0;
            if (difX<-50) _labelProximo.alpha = (-difX-50)/75;
            else          _labelProximo.alpha = 0;
            
            
        }
    }
    if (recognizer.state == UIGestureRecognizerStateEnded){
        if(toqueValido){
            CGFloat difX = _curLocation.x - _initLocation.x;
            if (difX>105) [self callPreviousCard:nil];
            else if(difX<-105) [self callNextCard:nil];
            else{
                [UIView animateWithDuration:0.1 animations:^{
                    _imagemCartaCima.center = posicaoCentro;
                    _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
                    _labelProximo.alpha = 0;
                    _labelAnterior.alpha = 0;
                }];
            }
            _labelProximo.alpha = 0;
            _labelAnterior.alpha = 0;
        }else{
            [UIView animateWithDuration:0.1 animations:^{
                _imagemCartaCima.center = posicaoCentro;
                _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
                _labelProximo.alpha = 0;
                _labelAnterior.alpha = 0;
            }];
        }
    }
    
}


#pragma funcoes
-(IBAction)callNextCard:(id*)sender{
    if(_card < 51){
        if(_animacaoFim){
            [UIView animateWithDuration:0.5 animations:^{
                _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
                _imagemCartaCima.center = posicaoForaEsquerda;
            }completion:^(BOOL finished){
                _imagemCartaCima.center = posicaoFora;
                _imagemCartaCima.image = [UIImage imageNamed:[_deckPrefix stringByAppendingString: [[_deckOfCards.deck objectAtIndex:++_card] nome]]];
                if(_card>0)
                    _imagemCartaBaixo.image = [UIImage imageNamed:[_deckPrefix stringByAppendingString:[[_deckOfCards.deck objectAtIndex:_card-1] nome]]];
                if(_card>_jogadas){
                    _jogadas++;
                    [_labelJogadas removeFromSuperview];
                }
                else
                    if(_jogadas!=0)
                        [self.view addSubview:_labelJogadas];
                if([[[_deckOfCards.deck objectAtIndex:_card] description] length] > 80)
                    _labelDescricao.font = [UIFont systemFontOfSize:19];
                else
                    if([[[_deckOfCards.deck objectAtIndex:_card] description] length] > 30)
                        _labelDescricao.font = [UIFont systemFontOfSize:21];
                    else
                        _labelDescricao.font = [UIFont systemFontOfSize:30];
                
                _labelJogadas.text = [NSString stringWithFormat:@"%d/%d",_card+1,_jogadas+1];
                _labelDescricao.text = [[_deckOfCards.deck objectAtIndex:_card] description];
                _animacaoFim = NO;
                [_imagemCartaBaixo setHidden:NO];
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:UIViewAnimationCurveEaseIn
                                 animations:^{
                                     _imagemCartaCima.frame = CGRectMake(_imagemCartaCima.frame.origin.x-240, _imagemCartaCima.frame.origin.y,_imagemCartaCima.frame.size.width,_imagemCartaCima.frame.size.height);
                                     _imagemCartaCima.transform = CGAffineTransformMakeRotation(1.5f);
                                     _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
                                 }
                                 completion:^(BOOL finished){
                                     _animacaoFim = YES;
                                     _imagemCartaBaixo.image = _imagemCartaCima.image;
                                 }];
            }];
            _imagemCartaBaixo.center = posicaoCentro;
            
            
            //FAZER O TEXTO DA FADE IN - OUT
            
            [UIView commitAnimations];
        }
    }
}

-(IBAction)callPreviousCard:(id*)sender{
    if(_card > 0){
        if(_animacaoFim){
            _imagemCartaCima.image = _imagemCartaBaixo.image;
            _card--;
            _imagemCartaBaixo.image = [UIImage imageNamed:[_deckPrefix stringByAppendingString: [[_deckOfCards.deck objectAtIndex:_card] nome]]];
            [self.view addSubview:_labelJogadas];
            
            if([[[_deckOfCards.deck objectAtIndex:_card] description] length] > 80)
                _labelDescricao.font = [UIFont systemFontOfSize:19];
            else
                if([[[_deckOfCards.deck objectAtIndex:_card] description] length] > 30)
                    _labelDescricao.font = [UIFont systemFontOfSize:21];
                else
                    _labelDescricao.font = [UIFont systemFontOfSize:30];
            
            _labelJogadas.text = [NSString stringWithFormat:@"%d/%d",_card+1,_jogadas+1];
            _labelDescricao.text = [[_deckOfCards.deck objectAtIndex:_card] description];
            
            [self animateExit];
        }
    }
}

-(IBAction)startButtonPress:(UIButton*)sender{
    
    [sender removeFromSuperview];
    _animacaoFim = YES;
    //[self.view addSubview:_imagemCartaBaixo];
    [_imagemCartaBaixo setHidden:YES];
    [self.view addSubview:_imagemCartaCima];
//    [self.view addSubview:_cartaBaixo];
//    [self.view addSubview:_cartaCima];
    [self callNextCard:nil];
    
}



-(IBAction)animateExit{
    _animacaoFim = NO;
    _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
    _imagemCartaCima.center = posicaoCentro;
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         _imagemCartaCima.frame = CGRectMake(_imagemCartaCima.frame.origin.x+240, _imagemCartaCima.frame.origin.y,_imagemCartaCima.frame.size.width,_imagemCartaCima.frame.size.height);
                         _imagemCartaCima.transform = CGAffineTransformMakeRotation(0);
                     }
                     completion:^(BOOL finished){
                         _animacaoFim = YES;
                     }];
    
        //FAZER O TEXTO DA FADE IN - OUT
    
    [UIView commitAnimations];
}

@end
