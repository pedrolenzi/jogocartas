//
//  SUECarta.h
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SUECarta : NSObject

@property (nonatomic) NSString* nome;
@property (nonatomic) NSString* descricao;

@end
