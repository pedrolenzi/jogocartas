//
//  SUEViewController.h
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUEDeck.h"

@interface SUEViewController : UIViewController <UIGestureRecognizerDelegate>

@property (nonatomic)UIImageView* imagemCartaCima;
@property (nonatomic)UIImageView* imagemCartaBaixo;
@property (nonatomic)UIView* cartaCima;
@property (nonatomic)UIView* cartaBaixo;
@property (nonatomic)UILabel* labelJogadas;
@property (nonatomic)UILabel* labelDescricao;
@property (nonatomic)SUEDeck* deckOfCards;
@property (nonatomic)NSString* deckPrefix;
@property int card,jogadas;
@property BOOL animacaoFim;

@end
