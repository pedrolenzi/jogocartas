//
//  SUEDeck.h
//  JogoSueca
//
//  Created by Pedro Nardino Lenzi on 1/10/14.
//  Copyright (c) 2014 Pedro Nardino Lenzi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SUECarta.h"

@interface SUEDeck : NSObject

@property (nonatomic) NSMutableArray* deck;
@property (nonatomic) NSArray* ordemValores;
@property (nonatomic) NSArray* ordemSuits;

-(void)setupCards;
-(instancetype)init;

@end
